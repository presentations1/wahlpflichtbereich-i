<h1>
  <a href="https://presentations1.gitlab.io/wahlpflichtbereich-i" style="font-size: 3em;">Wahl des Wahlpflichtmouls I</a>
</h1>

# Presentation:

- Präsentation lokal starten (über `node_modules/.bin/static` oder `npm start`)
- Präsentation auf [Gitlab](https://gitlab.com/presentations1/wahlpflichtbereich-i) läuft automatisch mit
  ([reveal.js | Multiplex](https://revealjs.com/multiplex/))

## Angewandte Chemie

- local: [`http:localhost:8000/ac.html`](http:localhost:8000/ac.html)
- remote: [`https://presentations1.gitlab.io/wahlpflichtbereich-i/ac.html#/sec-title-slide`](https://presentations1.gitlab.io/wahlpflichtbereich-i/ac.html#/sec-title-slide)
- short: [`https://kurzelinks.de/sao5">https://kurzelinks.de/om7y`](https://kurzelinks.de/sao5">https://kurzelinks.de/om7y)

## Pharmazeutische Chemie

- local: [`http:localhost:8000/pc.html`](http:localhost:8000/pc.html)
- remote: [`https://presentations1.gitlab.io/wahlpflichtbereich-i/pc.html#/sec-title-slide`](https://presentations1.gitlab.io/wahlpflichtbereich-i/pc.html#/sec-title-slide)
- short: [`https://kurzelinks.de/om7y">https://kurzelinks.de/om7y`](https://kurzelinks.de/om7y">https://kurzelinks.de/om7y)

## *Drug Discovery and Development*

- local: [`http:localhost:8000/ddd.html`](http:localhost:8000/ddd.html)
- remote: [`https://presentations1.gitlab.io/wahlpflichtbereich-i/ddd.html#/sec-title-slide`](https://presentations1.gitlab.io/wahlpflichtbereich-i/ddd.html#/sec-title-slide)
- short: [`https://kurzelinks.de/om7y">https://kurzelinks.de/dbfo`](https://kurzelinks.de/om7y">https://kurzelinks.de/dbfo)

## Auswahlseite

- Bitly: https://bit.ly/2Y7A0be

## Bearbeitung

- nur `index.html` bearbeiten, nicht die `.org`-Datei

# Prüfungsordnungen

## [Prüfungsordnung Pharmazeutische Chemie](https://www.th-koeln.de/mam/downloads/deutsch/studium/studiengaenge/f11/ordnungen_plaene/f11_bpo_ba_pcn_12.09.2018.pdf), [TH Köln](https://www.th-koeln.de/studium/pharmazeutische-chemie-bachelor_2446.php)


### § 24 (2)

Die Studierenden wählen vor Beginn des fünften Semesters eines der
Wahlpflichtmodule I „Pharmazeutische Technologie/pharmaceutics“,
“Pharmazeutische Chemie/small molecules”, “Bio-Pharmazeutische
Chemie/biological testing” und „Pharmazeutische
Biotechnologie/biologics“ (Wahlpflichtbereich).  

Mit ihrer Wahl beantragen die Studierenden die Zulassung zu dem
dazugehörigen Praktikum.


### § 24 (3)

Die Studierenden haben im Laufe des Studiums einmal die Möglichkeit,
das Wahlpflichtmodul I zu wechseln.  Dieser Wechsel soll im Rahmen des
regulären Wahlverfahrens beantragt werden.  Ein Wechsel kann
ausnahmsweise auch außerhalb des regulären Wahlverfahrens beantragt
werden. Ein solcher nachträglicher Wechsel ist nur in ein Wahlmodul I
mit freien Plätzen möglich. Ein Antrag auf nachträgliche Zulassung zum
Wahlpflichtmodul I muss vor dem Stichtag 31. August schriftlich beim
Prüfungsausschuss gestellt werden.

### § 24 (4)

Stehen in einem Wahlpflichtmodul I weniger Plätze zur Verfügung, als
Bewerberinnen und Bewerber dies zu belegen wünschen, so wird
entsprechend § 59 Abs. 2 HG bei der Vergabe wie folgt verfahren:

#### a)
Zunächst werden Studierende zugelassen, die das entsprechende
Wahlpflichtmodul I gewählt haben und zuvor nie zu einem
Wahlpflicht-modul I zugelassen waren oder im Wahlpflichtmodul I nie
ein Prüfungsergebnis „nicht bestanden“ erzielt oder die
Prüfungsleistung entschuldigt nicht erbracht haben.

#### b) 
Anschließend werden Studierende zugelassen, die das entsprechende
Wahlpflichtmodul I gewählt haben und bereits einmal zu einem
Wahl-pflichtmodul I zugelassen waren, es jedoch nicht bestanden
haben.

#### c) 
Anschließend werden Studierende zugelassen, die zu einem früheren
Zeitpunkt zu einem anderen Wahlpflichtmodul I zugelassen waren und das
Wahlpflichtmodul I wechseln wollen.

#### d)
Anschließend werden Studierende zugelassen, die bereits ein anderes
Wahlpflichtmodul I gewählt haben und dieses nicht wechseln wollen,
sowie Studierende, die bereits ein Wahlpflichtmodul I abgeschlossen
haben, und Studierende aus anderen Studiengängen.

Innerhalb der Gruppen a) bis d) werden die Studierenden nach der Zahl
der bereits erworbenen Leistungspunkte aus Modulen der ersten drei
Fachsemester gemäß Studienplan zugelassen, Stichtag 31. März; bei
Gleichheit nach diesem Kriterium entscheidet das höhere Fachsemester
und bei Gleichheit der Anzahl der Fachsemester entscheidet die bessere
Durchschnittsnote aus den bislang abgelegten Modulprüfungen. Bei
gleichem Rang entscheidet das Los.


## *Drug Discovery and Development*

- [Version 2](https://www.th-koeln.de/mam/downloads/deutsch/studium/studiengaenge/f11/ordnungen_plaene/f11_mpo_ddd_2023-03-28.pdf) vom 28. März 2023 
- § 24, Modulprüfungen, Wahlpflichmodul 1 mit Praktikumsanteil
