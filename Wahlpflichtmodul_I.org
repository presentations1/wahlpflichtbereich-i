#+Title: Wahlpflichtbereich I
#+Author: Richard Hirsch
#+Date: 14. Juni 2023
#+Language: de

* Org Stuff                                                        :noexport:
#+OPTIONS: toc:nil num:nil timestamp:nil

#+STARTUP: beamer
#+BEAMER_THEME: THK
#+OPTIONS: H:2
##+LaTeX_HEADER: \usepackage[ngerman]{babel}
#+LaTeX_HEADER: \institute{Pharmazeutische Chemie}

#+REVEAL_THEME: thk
#+REVEAL_ROOT: .
#+REVEAL_HLEVEL: 2
#+REVEAL_INIT_OPTIONS: center: false, slideNumber: false, hash: true, mouseWheel: true
#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="Wahlpflichtmodul 1">
#+REVEAL_POSTAMBLE: <p> Created by Richard Hirsch. </p>
#+REVEAL_PLUGINS: (markdown notes reveald3)

** LaTeX
#+LaTeX_HEADER: \DeclareUnicodeCharacter{2009}{\thinspace}
#+LaTeX_HEADER: \DeclareUnicodeCharacter{2892}{\newline}

#+LaTeX_HEADER: \newcounter{wish}
#+LaTeX_HEADER: \newcommand{\wish}{\makebox{\tiny
#+LaTeX_HEADER:   \arabic{wish}.~Wunsch}%
#+LaTeX_HEADER:   \stepcounter{wish}}
#+LaTeX_HEADER: \newcounter{dy}\setcounter{dy}{-8}
#+LaTeX_HEADER: \newcounter{ypos}
#+LaTeX_HEADER: \newcommand{\schwerpunkt}[1]{%
#+LaTeX_HEADER:   \put(0,\value{ypos}){
#+LaTeX_HEADER:   \put(0, 0){\makebox{#1}}
#+LaTeX_HEADER:   \multiput(68,0)(14,0){4}{\framebox(4,4){}}}
#+LaTeX_HEADER:   \addtocounter{ypos}{\value{dy}}
#+LaTeX_HEADER: }
   

* Wahlpflichtmodul 1

** COMMENT Wahlpflichtmodul 1 (§ 24 Abs. 1 PO4)

Die Studierenden wählen vor Beginn des fünften Semesters eines der
Wahlpflichtmodule I 
- „Pharmazeutische Technologie, /pharmaceutics/“
- „Pharmazeutische Chemie, /small molecules/”
- „Bio-Pharmazeutische Chemie, /biological testing/”
- „Pharmazeutische Biotechnologie, /biologics/“

Mit ihrer Wahl beantragen die Studierenden die Zulassung zu dem
dazugehörigen Praktikum.

** Wahlpflichtmodul 1 (§ 24 Abs. 1 PO4)

Die Studierenden wählen vor Beginn des fünften Semesters eines der
Wahlpflichtmodule I 
- „Pharmazeutische Technologie, /pharmaceutics/“
- „Pharmazeutische Chemie, /small molecules/“
- „Bio-Pharmazeutische Chemie, /biological testing/“
- „Pharmazeutische Biotechnologie, /biologics/“

Mit ihrer Wahl beantragen die Studierenden die Zulassung zu dem
dazugehörigen Praktikum.

** Implikationen der Wahl

- Zulassung zum dazugehörigen Praktikum
- Wahlpflichtmodul I wird als Modul auf dem *Bachelorzeugnis* ausgewiesen
- taucht *nicht* auf der *Bachelorurkunde* auf
- schränkt Sie *nicht* in der Wahl des Themenbereichs für Ihre *Bachelorarbeit* ein

* Praktikum im Wahlpflichtmodul 1

** Zulassung zum dazugehörigen Praktikum

- Anzahl der Plätze in den Praktika begrenzt

- Zuteilungsmodus in der Prüfungsordnung (§ 24) geregelt

  1. Erst- oder Folgewahl
  2. Anzahl der Leistungspunkte⮒\newline
     (aus den Fachsemestern 1–3, Stichtag: 30. April 2023 )
  3. Fachsemester
  4. Durchschnittsnote
  5. Los

#+LaTeX: \bigskip     
*** Erstwunsch nicht erfüllbar?

Wenn Sie keinen Platz in Ihrem Wunschfach bekommen:

#+LaTeX: \pause\bigskip
1. anderes Fach
2. neue Chance bei nächster Wahl


** Alternative 1: anderes Praktikum

1. Sie geben auf Ihrem Wahlzettel einen Zweitwunsch an⮒\newline
   (und ggf. Drittwunsch):
   - wenn möglich: Zuteilung beim Zweitwunsch, (Drittwunsch)
   - garantierter Platz bei Angabe von vier verschiedenen
     Fächern
2. Sie wählen bis zum 31. August einen Schwerpunkt mit
   offenen Plätzen nach.
   

** Alternative 2: Neuwahl

- Sie werden keinem Praktikum zugeteilt, das Sie nicht ausdrücklich
  gewählt haben.

- Sie nehmen an der Wahl im nächsten Sommersemester
  als Erstwählerin/Erstwähler teil.⮒\newline
  (mehr Leistungspunkte, höheres Fachsemester, also höhere Priorität).


* Wahl des Wahlpflichtmoduls I

** Zulassung

*** Teilnahme an der Wahl

- Jede und jeder Studierende aus dem Studiengang Pharmazeutische
  Chemie ist automatisch zur Wahl zugelassen.

- Regelzeitpunkt: 4. Fachsemester

*** Teilnahme am Praktikum

- Praktikumsmodule der Semester 1 und 4 müssen bestanden sein.

** Wahl des Wahlpflichtmoduls 1

**** Wahlzeitraum: 
#+BEGIN_CENTER
*15.06.* 12:00 Uhr -- *22.06.* 12:00 Uhr
#+END_CENTER

**** Wahlhandlung
#+LaTeX: \medskip
- Sie füllen im PC-Pool einen Wahlzettel *online* aus,
  + ILIAS-Ordner *Wahlpflichbereich 1*
- Sie drucken den Wahlzettel aus und reichen ihn *unterschrieben*
  per E-Mail bei Frau Kobs ein (=emily.kobs@th-koeln.de=).
#+LaTeX: \par\pause\smallskip
- Wenn Sie sich umentscheiden, wählen Sie einfach neu;⮒\newline
  es gilt der jeweils neuste Wahlzettel⮒\newline
  (ggf. Uhrzeit auf dem Wahlzettel vermerken)

** Wahlzettel

#+BEGIN_EXPORT latex
\hrule\par\medskip
\unitlength1mm
  Ich bewerbe mich um die Teilnahme am Vertiefungspraktikum des
  Studienschwerpunkts
  \par
  \emph{\small (Geben Sie jeweils nur einen 1.~Wunsch,
    einen 2.~Wunsch, einen 3. und einen 4.~Wunsch an.)}
%
  \begin{center}
    \setcounter{wish}{1}
    \setcounter{ypos}{35}
    \begin{picture}(150,40)(0,0)
      \multiput(63,42)(14,0){4}{\wish}
      \schwerpunkt{Pharmazeutische Chemie \& Analytik}
      \schwerpunkt{Pharmazeutische Technologie}
      \schwerpunkt{Bio-pharmazeutische Chemie}
      \schwerpunkt{Pharma Management}

      \put(0, 0){\makebox{Datum:\quad}\line(1,0){28}\qquad
        \makebox{Unterschrift:\quad}\line(1,0){40}}
    \end{picture}
  \end{center}
\hrule
#+END_EXPORT


** Ausfüllen der Wahlzettel

- Ein Kreuz in der Spalte „1. Wunsch“ für den gewünschten
  Wahlpflichtbereich

- ggf. weitere Kreuze für Zweit-, Dritt- und Viertwunsch

- wenn zutreffend, ankreuzen, dass Sie noch nie zu einem
  Praktikum im Wahlpflichtbereich I an dieser Fakultät zugelassen wurden

- *Absenden* nicht vergessen!


** COMMENT Abgabe der Wahlzettel

- bis *\electionStop* 16:00 Uhr im Sekretariat (Wahlurne)

\bigskip
- Vergessen Sie nicht Ihre *Unterschrift*!


** Auszählung und Zuteilung

- nicht-öffentliche Auszählung und Zuteilung (Datenschutz)
  + Studierendenvertreter aus dem Prüfungsausschuss sind anwesend.

- Bekanntgabe der Ergebnisse durch Aushang


* Probleme

** Ungültige Stimmzettel

- Ihre Willensbekundung ist nicht eindeutig erkennbar.
- Sie haben einen Zusatz oder Vorbehalt ergänzt.
- Sie haben den Wahlzettel nicht unterschrieben.

#+LaTeX: \bigskip
#+LaTeX: \pause
- Sie haben sich umentschieden.

Sie können nachwählen oder wechseln (s. u.)


** Nachträgliche Wahl

- Sie beantragen beim Prüfungsausschuss die Zuteilung zu einem
  Praktikum mit freien Plätzen (nach Maßgabe der PO)

- Frist: 31. August

- Die nachträgliche Wahl gilt als Wechsel des Wahlpflichtmoduls I


** Sie möchten das Wahlpflichtmodul I wechseln

- Sie beantragen beim Prüfungsausschuss die Zuteilung zu einem
  Praktikum mit freien Plätzen (nach Maßgabe der PO)

- Frist: 31. August

- Wechsel des Wahlpflichtmoduls I nur einmal möglich!


** Vielen Dank

#+LaTeX: \vfill
#+begin_center
für Ihre Aufmerksamkeit
#+end_center
#+LaTeX: \vfill

